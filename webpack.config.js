'use strict';

const webpack           = require('webpack');
const nodeExternals = require('webpack-node-externals');
const path              = require( 'path' );

module.exports = {
  entry: './App.ts',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    // alias: {
    //   ['@']: path.resolve(__dirname, 'src'),
    // }
  },
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?100'],
    }),
  ],
  node: { fs: 'empty' },
  module: {
    // loaders: [
    //   { test: /\.tsx?$/, loader: 'ts-loader' }
    // ],
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
    ]
  },
  plugins: [
  ]
}
