import { CommentModel, QuestionModel, SegmentModel, UserModel } from "../models";

export const seedDB = async () => {
  await UserModel.bulkCreate([
    {
      nickname: 'udachin',
      permission: 'admin',
      password: 'qwerty',
    },
    {
      nickname: 'pip',
      permission: 'user',
      password: '123qwe',
    },
  ])

  await SegmentModel.bulkCreate([
    {
      name: 'IT'
    },
    {
      name: 'MosPolytech'
    }
  ])

  await QuestionModel.bulkCreate([
    {
      title: 'Как сдать сессию?',
      text: 'Ну вот не сдается и все. Что делать??',
      userId: 1,
      segmentId: 2,
      isPublished: true,
      mark: 100,
      date: Date.now(),
    },
    {
      title: 'Хачу работать',
      text: 'Умею мыть полы и обучать ML модели',
      userId: 2,
      segmentId: 1,
      isPublished: true,
      mark: 3,
      date: Date.now(),
    },
    {
      title: 'Сайт у вас не очень!',
      text: 'troll',
      userId: 2,
      segmentId: 2,
      isPublished: false,
      mark: 0,
      date: Date.now(),
    },
  ])

  await CommentModel.bulkCreate([
    {
      text: 'Very easy',
      questionId: 1,
      userId: 2,
      date: Date.now(),
    }
  ]);
};
