// user

import express from 'express';
import {
  requestAuthUser,
  requestChangePermissionUser,
  requestCreateUser,
  requestDeleteUser,
  requestFindAllUsers,
  requestFindOneUser,
} from "../requests/user";
const router = express.Router();

router.post('/create', requestCreateUser);
router.delete('/:id', requestDeleteUser);
router.post('/auth', requestAuthUser);
router.put('/permission', requestChangePermissionUser);
router.get('/:id', requestFindOneUser);
router.get('/', requestFindAllUsers);

export default router;
