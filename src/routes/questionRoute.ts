// question

import express from 'express';
import {
  requestCreateQuestion,
  requestDeleteQuestion,
  requestFindAllQuestion,
  requestMarkQuestion,
  requestPublishQuestion,
  requestFindQuestion,
} from "../requests/question";
const router = express.Router();

router.post('/create', requestCreateQuestion);
router.delete('/:id', requestDeleteQuestion);
router.put('/publish', requestPublishQuestion);
router.put('/mark', requestMarkQuestion);
router.get('/:id', requestFindQuestion);
router.get('/', requestFindAllQuestion);

export default router;
