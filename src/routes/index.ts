import segmentRoute from './segmentRoute';
import commentRoute from './commentRoute';
import questionRoute from './questionRoute';
import userRoute from './userRoute';

export default [
    {path: '/comment', route: commentRoute},
    {path: '/question', route: questionRoute},
    {path: '/segment', route: segmentRoute},
    {path: '/user', route: userRoute},
]

