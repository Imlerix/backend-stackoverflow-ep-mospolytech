// segment
import express from 'express';
import { requestCreateSegment, requestRenameSegment, requestFindAllSegment } from "../requests/segment";
const router = express.Router();

router.post('/create', requestCreateSegment);
router.put('/', requestRenameSegment);
router.get('/', requestFindAllSegment);

export default router;
