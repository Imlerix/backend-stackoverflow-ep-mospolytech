// comment

import express from 'express';
import { requestCreateComment, requestDeleteComment, getAllComment } from "../requests/comment";
const router = express.Router();

router.post('/create', requestCreateComment);
router.delete('/:id', requestDeleteComment);
router.get('/:questionId', getAllComment);

export default router;
