const corsOptions = {
    origin: '*',
    credentials: true,
    allowedHeaders: [ 'Content-Type', 'Authorization' ]
}

const logger = require('morgan');
const combinedlogger = logger(('combined'));
// import customLogger from './logger';
const bodyParserJson = require('body-parser').json();
const bodyParserUrl = require('body-parser').urlencoded({extended: true});
const fileUpload = require('express-fileupload');
const cors = require('cors')(corsOptions);
// const errors = require('./errors');

export default [
    combinedlogger,
    bodyParserJson,
    bodyParserUrl,
    cors,
    fileUpload({
        createParentPath: true
    }),
    // customLogger,
    // errors
];
