import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Model,
    PrimaryKey,
    Table
} from "sequelize-typescript";
import sequelize from "../lib/db";
import { User } from "./user.model";
import { Segment } from "./segment.model";

@Table({
    modelName: 'question'
})
export class Question extends Model<File> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    title: string;

    @AllowNull(false)
    @Column(DataType.TEXT)
    text: string;

    @AllowNull(false)
    @Column(DataType.BOOLEAN)
    isPublished: boolean;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    mark: number;

    // user
    @ForeignKey(() => User)
    @Column
    userId: number;

    @BelongsTo(() => User)
    user: User;

    // segment
    @ForeignKey(() => Segment)
    @Column
    segmentId: number;

    @BelongsTo(() => Segment)
    segment: Segment;

    @AllowNull(false)
    @Column(DataType.DATEONLY)
    date: Date;
}

