import sequelize from "../lib/db";
import { Comment } from "./comment.model";
import { User } from "./user.model";
import { Question } from "./question.model";
import { Segment } from "./segment.model";

export { User as UserModel};
export { Question as QuestionModel};
export { Segment as SegmentModel};
export { Comment as CommentModel};

sequelize.addModels([Comment, User, Question, Segment]);
