import { AllowNull, AutoIncrement, BelongsTo, Column, DataType, Model, PrimaryKey, Table } from "sequelize-typescript";
import sequelize from "../lib/db";

@Table({
    modelName: 'user'
})
export class User extends Model<File> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id: number;

    @AllowNull(false)
    @Column(DataType.TEXT)
    nickname: string;

    @AllowNull(false)
    @Column(DataType.TEXT)
    permission: string;

    @AllowNull(false)
    @Column(DataType.TEXT)
    password: string;
}

