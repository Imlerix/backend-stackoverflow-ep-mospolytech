import { CommentModel, UserModel } from '../../models'

export const getAllCommentByQuestionId = async (questionId: number) => {

  return CommentModel.findAll({
    where: {
      questionId
    },
    include: [
      {
        model: UserModel,
        attributes: ['id', 'nickname']
      }
    ]
  });
};
