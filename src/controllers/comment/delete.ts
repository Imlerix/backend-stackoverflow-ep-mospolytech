import { CommentModel } from '../../models';

type ParamsType = {
  id: number
};

export const deleteComment = async ({ id }: ParamsType) => {

  const comment = await CommentModel.findOne({
    where: {
      id
    }
  });

  if (!comment) {
    return Promise.reject({
      message: 'Комментарий не найден.'
    })
  }

  return CommentModel.destroy({
    where: {
      id
    }
  });
};
