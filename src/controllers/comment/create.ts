import { CommentModel } from '../../models'

export const createComment = async ({userId, questionId, text}: CommentModel) => {

  return CommentModel.create({
    userId,
    questionId,
    text,
    date: Date.now(),
  });
};
