import { SegmentModel } from '../../models'

export const createSegment = async ({name}: SegmentModel) => {

  return SegmentModel.create({
    name,
  }, { fields: [ 'id', 'name' ] });
};
