import { SegmentModel } from '../../models';

export const renameSegment = async ({ id, name }: SegmentModel) => {

  const segment = await SegmentModel.findOne({
    where: {
      id
    }
  });

  if (!segment) {
    throw new Error('Не найдена рубрика');
  }

  return SegmentModel.update({
    name,
  }, {
    where: {
      id
    }
  });
};
