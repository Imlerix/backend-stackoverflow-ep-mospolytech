import { UserModel } from '../../models'
import { checkUserPermission } from "../../../helpers/checkUserPermission";

export const changePermissionUser = async ({id, permission}: UserModel) => {

  checkUserPermission(permission);

  const user = UserModel.findOne({
    where: {
      id
    }
  });

  if (!user) {
    throw new Error('Пользователь не найден')
  }

  return UserModel.update({permission}, {
    where: {
      id
    }
  })
};
