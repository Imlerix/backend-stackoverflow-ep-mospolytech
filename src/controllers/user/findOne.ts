import { UserModel } from '../../models'

type ParamsType = {
  id: number,
};

export const findOne = async ({id}: ParamsType) => {

  return UserModel.findOne({
    where: {
      id
    },
    attributes: ['id', 'nickname', 'permission']
  });
};
