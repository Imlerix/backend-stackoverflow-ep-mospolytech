import { UserModel } from '../../models'

export const authUser = async ({password, nickname}: UserModel) => {

  return UserModel.findOne({
    where: {
      password,
      nickname,
    }
  });
};
