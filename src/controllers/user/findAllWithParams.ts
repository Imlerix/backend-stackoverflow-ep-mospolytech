import { UserModel } from '../../models'

type ParamsType = {
  search?: string,
};

export const findAllUsersWithParams = async ({search}: ParamsType) => {

  let where: any = {};

  if (search) {
    where.nickname = {
      $like: `%${search}%`
    }
  }

  return UserModel.findAll({
    where,
    attributes: ['id', 'nickname', 'permission']
  });
};
