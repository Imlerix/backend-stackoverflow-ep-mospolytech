import { UserModel } from '../../models'

type ParamsType = {
  id: number
};

export const deleteUser = async ({ id }: ParamsType) => {

  return UserModel.destroy({
    where: {
      id
    }
  });
};
