import { UserModel } from '../../models'

export const createUser = async ({nickname, permission, password}: UserModel) => {

  return UserModel.create({
    nickname,
    permission,
    password,
  }, {
    fields: [ 'id', 'nickname', 'permission', 'password' ]
  });
};
