import { QuestionModel } from '../../models'

type ParamsType = {
  id: number
};

export const deleteQuestion = async ({ id }: ParamsType) => {

  return QuestionModel.destroy({
    where: {
      id
    }
  });
};
