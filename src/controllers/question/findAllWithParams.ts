import { QuestionModel, SegmentModel, UserModel } from '../../models'

type ParamsType = {
  userId?: number,
  segmentId?: number,
  search?: string,
  isPublished?: boolean,
};

export const findAllWithParams = async (
  {
    userId,
    segmentId,
    search,
    isPublished,
  }: ParamsType) => {

  const where: any = {};
  const include = [
    {
      model: UserModel,
      attributes: [ 'id', 'nickname' ]
    },
    {
      model: SegmentModel,
    }
  ];

  if (userId) {
    where.userId = userId;
  }

  if (segmentId) {
    where.segmentId = segmentId;
  }

  if (search) {
    where.$or = {
      title: {
        $like: `%${search}%`
      },
      text: {
        $like: `%${search}%`
      },
    }
  }

  if (isPublished === true) {
    where.isPublished = true;
  }

  return QuestionModel.findAll({
    where,
    include,
  });
};
