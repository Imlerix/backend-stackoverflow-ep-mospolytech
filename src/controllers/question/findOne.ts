import { QuestionModel, SegmentModel, UserModel } from '../../models'

type ParamsType = {
  id: number,
};

export const findOneQuestion = async ({id}: ParamsType) => {

  const include = [
    {
      model: UserModel,
      attributes: [ 'id', 'nickname' ]
    },
    {
      model: SegmentModel,
    }
  ];

  return QuestionModel.findOne({
    where: {
      id
    },
    include,
  });
};
