import { QuestionModel } from '../../models'

type ParamsType = {
  id: number,
};

export const markQuestion = async ({ id }: ParamsType) => {

  const question = await QuestionModel.findOne({
    where: {
      id
    }
  });

  if (!question) {
    throw new Error('Не найден вопрос');
  }

  return QuestionModel.update({
    mark: question.mark + 1,
  }, {
    where: {
      id
    }
  });
};
