import { QuestionModel } from '../../models'

export const createQuestion = async (question: QuestionModel) => {

  return QuestionModel.create({
    ...question,
    isPublished: false,
    date: Date.now(),
    mark: 0,
  });
};
