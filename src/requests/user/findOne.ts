import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";
import { findOne } from "../../controllers/user/findOne";

export const requestFindOneUser = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.id) {
      throw new Error('Не все параметры переданы (id)');
    }

    const user = await findOne({id: Number(req.params.id)});

    res.status(200).json(answer200({
      data: {
        id: user.id,
        nickname: user.nickname,
        permission: user.permission,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
