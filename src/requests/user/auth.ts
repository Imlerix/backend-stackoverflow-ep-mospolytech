import { authUser } from "../../controllers/user";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

const reqParams = [
  'nickname',
  'password',
];

export const requestAuthUser = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не все параметры переданы');
      }
    });

    const user = await authUser(req.body);

    res.status(200).json(answer200({
      data: {
        id: user.id,
        nickname: user.nickname,
        permission: user.permission,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
