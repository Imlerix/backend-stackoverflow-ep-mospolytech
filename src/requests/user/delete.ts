import { deleteUser } from "../../controllers/user";
import { Request, Response, NextFunction } from 'express'
import { answer200 } from "../_constants";

export const requestDeleteUser = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.id) {
      throw new Error('Не передан id юзера');
    }

    await deleteUser({ id: Number(req.params.id) });

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
