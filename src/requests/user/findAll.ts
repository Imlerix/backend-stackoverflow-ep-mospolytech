import { findAllUsersWithParams } from "../../controllers/user";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

export const requestFindAllUsers = async(req: Request, res: Response, next: NextFunction) => {
  try{

    const users = await findAllUsersWithParams(req.query);

    res.status(200).json(answer200({
      data: {
        users
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
