import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";
import { changePermissionUser } from "../../controllers/user/changePermission";

const reqParams = [
  'id',
  'permission',
];

export const requestChangePermissionUser = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не все параметры переданы');
      }
    });

    await changePermissionUser(req.body);

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
