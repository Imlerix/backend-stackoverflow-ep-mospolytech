import { createUser } from "../../controllers/user";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";
import { checkUserPermission } from '../../../helpers/checkUserPermission'

const reqParams = [
  'nickname',
  'password',
  'permission',
];

export const requestCreateUser = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не все параметры переданы');
      }
    });

    checkUserPermission(req.body.permission);

    const user = await createUser(req.body);

    res.status(200).json(answer200({
      data: {
        id: user.id,
        nickname: user.nickname,
        permission: user.permission,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
