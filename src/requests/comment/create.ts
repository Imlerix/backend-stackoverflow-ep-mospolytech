import { createComment } from "../../controllers/comment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

const reqParams = [
  'text',
  'questionId',
  'userId',
];

export const requestCreateComment = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не переданы параметры');
      }
    });

    const comment = await createComment(req.body);

    res.status(200).json(answer200({
      data: {
        comment,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
