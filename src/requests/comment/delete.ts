import { deleteComment } from "../../controllers/comment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

export const requestDeleteComment = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.id) {
      throw new Error('Не переданы параметры: id');
    }

    await deleteComment({id: Number(req.params.id)});

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
