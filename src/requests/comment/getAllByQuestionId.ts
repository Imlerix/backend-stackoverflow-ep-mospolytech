import { getAllCommentByQuestionId } from "../../controllers/comment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

export const getAllComment = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.questionId) {
      throw new Error('Не переданы параметры: questionId');
    }

    const comments = (await getAllCommentByQuestionId(Number(req.params.questionId))) || [];

    res.status(200).json(answer200({
      data: {
        comments
      },
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
