import { findAllWithParams } from "../../controllers/question";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

export const requestFindAllQuestion = async(req: Request, res: Response, next: NextFunction) => {
  try{

    const questions = await findAllWithParams(req.query);

    res.status(200).json(answer200({
      data: {
        questions
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
