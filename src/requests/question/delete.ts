import { deleteQuestion } from "../../controllers/question";
import { Request, Response, NextFunction } from 'express'
import { answer200 } from "../_constants";

export const requestDeleteQuestion = async(req: Request, res: Response, next: NextFunction) => {
  try{
    if (!req.params.id) {
      throw new Error('Не передан id файла');
    }

    await deleteQuestion({ id: Number(req.params.id) });

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
