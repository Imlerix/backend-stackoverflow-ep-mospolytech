import { findOneQuestion } from "../../controllers/question";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";


export const requestFindQuestion = async(req: Request, res: Response, next: NextFunction) => {
  try{

    if (!req.params.id) {
      throw new Error('Не переданы параметры');
    }

    const question = await findOneQuestion({id: Number(req.params.id)});

    res.status(200).json(answer200({
      data: question,
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
