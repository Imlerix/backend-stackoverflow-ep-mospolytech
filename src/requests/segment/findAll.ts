import { findAllSegment } from "../../controllers/segment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

export const requestFindAllSegment = async(req: Request, res: Response, next: NextFunction) => {
  try{

    const segments = (await findAllSegment()) || [];

    res.status(200).json(answer200({
      data: {
        segments,
      }
    }));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
