import { renameSegment } from "../../controllers/segment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

const reqParams = [
  'name',
  'id',
];

export const requestRenameSegment = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не переданы параметры');
      }
    });

    await renameSegment(req.body);

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
