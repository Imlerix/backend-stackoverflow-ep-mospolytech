import { createSegment } from "../../controllers/segment";
import { Request, Response, NextFunction } from 'express';
import { answer200 } from "../_constants";

const reqParams = [
  'name'
];

export const requestCreateSegment = async(req: Request, res: Response, next: NextFunction) => {
  try{
    reqParams.forEach(item => {
      if (!req.body[item]) {
        throw new Error('Не передано имя');
      }
    });

    const segment = await createSegment(req.body);

    res.status(200).json(answer200({}));

  }catch (e) {
    res.status(500).json(answer200({error: true, errorText: e.message}))
  }
  next()
};
