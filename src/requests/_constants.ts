type ParamsAnswer200 = {
  error?: boolean;
  errorText?: string;
  additionalErrors?: string[];
  data?: any;
};

export const answer200 = ({ error, data, errorText }: ParamsAnswer200) => ({
  error: error || false,
  errorText: errorText || '',
  // @ts-ignore
  additionalErrors: [],
  data: data || null,
});
