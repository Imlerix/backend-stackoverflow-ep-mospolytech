# About
Аналог stackoverflow (бекенд) на Node.js, Express, Postrgres, Sequelize и Docker.
Сделан Голенко Вадимом для инженерного проектирования Мосполитеха.

# Server
http://stackoverflow.udachin.tech

# Documentation API

#### Комментарии

##### Получить список комментов
URI: `GET /comment/:questionId`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {
        comments: [
            {
                id: number,
                text: string,
                userId: 2,
                questionId: 1,
                date: "2020-07-10", // string
                user: {
                    id: number,
                    nickname: string
                }
            }
        ]
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Добавить коммент
URI: `POST /comment/create`

request body: 
```
JSON:
{
    text: string,
    questionId: number,
    userId: number,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Удалить коммент
URI: `DELETE /comment/:id`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```


#### Вопросы

##### Создать вопрос
URI: `POST /question/create`

request body: 
```
JSON:
{
    title: string,
    text: string,
    userId: number,
    segmentId: number,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Удалить вопрос
URI: `DELETE /question/:id`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Одобрить вопрос
URI: `PUT /question/publish`

request body: 
```
JSON:
{
    id: number,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Оценить вопрос (к текущему значинию просто прибавляется +1)
URI: `PUT /question/mark`

request body: 
```
JSON:
{
    id: number,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Получить вопрос
URI: `GET /question/:id`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {
        id: number,
        title: string,
        text: string,
        isPublished: boolean,
        mark: number,
        userId: number,
        segmentId: number,
        date: "2020-07-10", // string
        user: {
            id: number,
            nickname: string
        },
        segment: {
            id: number,
            name: string
        }
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Получить список вопросов
URI: `GET /question?<имя квери>=<значение квери>&<имя квери>=<значение квери>`

request query: 
<br>
необязательные помечены вопросиком (?)
```
  userId?: number,
  segmentId?: number,
  search?: string, // ищет по title или text
  isPublished?: boolean, // если true то присылаются только одобренные(isPublished: true)
```

response body: 
```
JSON:
{
    data: {
        questions: [
            id: number,
            title: string,
            text: string,
            isPublished: boolean,
            mark: number,
            userId: number,
            segmentId: number,
            date: "2020-07-10", // string
            user: {
                id: number,
                nickname: string
            },
            segment: {
                id: number,
                name: string
            }
        ]
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

#### Рубрики вопросов

##### Создать рубрику
URI: `POST /segment/create`

request body: 
```
JSON:
{
    name: string,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Изменить рубрику
URI: `PUT /segment`

request body: 
```
JSON:
{
    id: number,
    name: string,
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Получить список рубрик
URI: `GET /segment`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {
        segments: [
            {
                id: number,
                name: string,
            }
        ]
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

#### Юзеры

##### Создать юзера
URI: `POST /user/create`

request body: 
```
JSON:
{
    nickname: string,
    password: string,
    permission: string, // 'admin' | 'manager' | 'user'
}
```

response body: 
```
JSON:
{
    data: {
        id: number,
        nickname: string,
        permission: string, // 'admin' | 'manager' | 'user'
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Удалить юзера
URI: `DELETE /user/:id`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Авторизация юзера
URI: `POST /user/auth`

request body: 
```
JSON:
{
    nickname: string,
    password: string,
}
```

response body: 
```
JSON:
{
    data: {
        id: number,
        nickname: string,
        permission: string, // 'admin' | 'manager' | 'user'
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Сменить роль юзера
URI: `PUT /user/permission`

request body: 
```
JSON:
{
    id: number,
    permission: string, // 'admin' | 'manager' | 'user'
}
```

response body: 
```
JSON:
{
    data: {},
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Получить список юзеров
URI: `GET /user?search=somestroka`

request query: 
```
search?: string // ищет по nickname
```

response body: 
```
JSON:
{
    data: {
        users: [
            {
                id: number,
                nickname: string,
                permission: string, // 'admin' | 'manager' | 'user'
            }
        ]
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```

##### Получить данные о юзере
URI: `GET /user/:id`

request body: 
```
none
```

response body: 
```
JSON:
{
    data: {
        id: number,
        nickname: string,
        permission: string, // 'admin' | 'manager' | 'user'
    },
    error: false, // boolean
    errorText: '', // string
    additionalErrors: [], // string[]
}
```
