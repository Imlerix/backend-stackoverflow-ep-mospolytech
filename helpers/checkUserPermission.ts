export const checkUserPermission = (permission: string) => {
  if ( !(permission === 'admin' || permission === 'manager' || permission === 'user' ) ) {
    throw new Error(`Нет такого типа: ${permission}`);
  }
};
